#define _CRT_SECURE_NO_DEPRECATE

#include <CL/cl.h>

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Matrix.h"

//#define SHOW_PLATFORM_DEVICES_INFO
#define MAX_SOURCE_SIZE 100000

#define CL_CHECK(_expr) \
	do { \
		cl_int _err = _expr;                                                       \
		if (_err == CL_SUCCESS)                                                    \
			break;                                                                   \
		fprintf(stderr, "OpenCL Error: '%s' returned %d!\n", #_expr, (int)_err);   \
		abort();                                                                   \
	} while (0)

#define CL_CHECK_ERR(error) \
	if (error != CL_SUCCESS) {\
		printf("\n Error number %d", error);\
	}

//some callback, not sure what it does
void pfn_notify(const char *errinfo, const void *private_info, size_t cb, void *user_data)
{
	fprintf(stderr, "OpenCL Error (via pfn_notify): %s\n", errinfo);
}

float mabs(float a) {
	if (a < 0)
		return -a;
	return a;
}

int main(int argc, char **argv)
{
	char resultsFileName[] = "test.dat";
	remove(resultsFileName);

	FILE *fp;
	//file with kernel code
	const char sourceFileName[] = "kernel.cl";
	size_t source_size;
	char *source_str;
	/* Load kernel source code */
	fp = fopen(sourceFileName, "r");
	if (!fp) {
	}
	source_str = (char *)malloc(MAX_SOURCE_SIZE);
	source_size = fread(source_str, 1, MAX_SOURCE_SIZE, fp);
	fclose(fp);

	cl_platform_id platforms[100];
	cl_uint platforms_n = 0;
	cl_int _err;

	CL_CHECK(clGetPlatformIDs(100, platforms, &platforms_n));

#ifdef SHOW_PLATFORM_DEVICES_INFO
	printf("=== %d OpenCL platform(s) found: ===\n", platforms_n);
	for (int i = 0; i<platforms_n; i++)
	{
		char buffer[10240];
		printf("  -- %d --\n", i);
		CL_CHECK(clGetPlatformInfo(platforms[i], CL_PLATFORM_PROFILE, 10240, buffer, NULL));
		printf("  PROFILE = %s\n", buffer);
		CL_CHECK(clGetPlatformInfo(platforms[i], CL_PLATFORM_VERSION, 10240, buffer, NULL));
		printf("  VERSION = %s\n", buffer);
		CL_CHECK(clGetPlatformInfo(platforms[i], CL_PLATFORM_NAME, 10240, buffer, NULL));
		printf("  NAME = %s\n", buffer);
		CL_CHECK(clGetPlatformInfo(platforms[i], CL_PLATFORM_VENDOR, 10240, buffer, NULL));
		printf("  VENDOR = %s\n", buffer);
		CL_CHECK(clGetPlatformInfo(platforms[i], CL_PLATFORM_EXTENSIONS, 10240, buffer, NULL));
		printf("  EXTENSIONS = %s\n", buffer);
	}
#endif

	if (platforms_n == 0)
		return 1;

	cl_device_id devices[100];
	cl_uint devices_n = 0;

	//TODO: we should get the right device -should be working for now
	CL_CHECK(clGetDeviceIDs(platforms[1], CL_DEVICE_TYPE_GPU, 100, devices, &devices_n));

#ifdef SHOW_PLATFORM_DEVICES_INFO
	printf("=== %d OpenCL device(s) found on platform:\n", platforms_n);
	for (int i = 0; i<devices_n; i++)
	{
		char buffer[10240];
		cl_uint buf_uint;
		cl_ulong buf_ulong;
		printf("  -- %d --\n", i);
		CL_CHECK(clGetDeviceInfo(devices[i], CL_DEVICE_NAME, sizeof(buffer), buffer, NULL));
		printf("  DEVICE_NAME = %s\n", buffer);
		CL_CHECK(clGetDeviceInfo(devices[i], CL_DEVICE_VENDOR, sizeof(buffer), buffer, NULL));
		printf("  DEVICE_VENDOR = %s\n", buffer);
		CL_CHECK(clGetDeviceInfo(devices[i], CL_DEVICE_VERSION, sizeof(buffer), buffer, NULL));
		printf("  DEVICE_VERSION = %s\n", buffer);
		CL_CHECK(clGetDeviceInfo(devices[i], CL_DRIVER_VERSION, sizeof(buffer), buffer, NULL));
		printf("  DRIVER_VERSION = %s\n", buffer);
		CL_CHECK(clGetDeviceInfo(devices[i], CL_DEVICE_MAX_COMPUTE_UNITS, sizeof(buf_uint), &buf_uint, NULL));
		printf("  DEVICE_MAX_COMPUTE_UNITS = %u\n", (unsigned int)buf_uint);
		CL_CHECK(clGetDeviceInfo(devices[i], CL_DEVICE_MAX_CLOCK_FREQUENCY, sizeof(buf_uint), &buf_uint, NULL));
		printf("  DEVICE_MAX_CLOCK_FREQUENCY = %u\n", (unsigned int)buf_uint);
		CL_CHECK(clGetDeviceInfo(devices[i], CL_DEVICE_GLOBAL_MEM_SIZE, sizeof(buf_ulong), &buf_ulong, NULL));
		printf("  DEVICE_GLOBAL_MEMSIZE = %llu\n", (unsigned long long)buf_ulong);
	}
#endif 

	if (devices_n == 0)
		return 1;

	cl_context context;
	context = clCreateContext(NULL, devices_n, devices, &pfn_notify, NULL, &_err);
	CL_CHECK_ERR(_err);

	cl_program program;
	program = clCreateProgramWithSource(context, 1, (const char **)&source_str, (const size_t *)&source_size, &_err);
	CL_CHECK_ERR(_err);
	if (clBuildProgram(program, 1, devices, "", NULL, NULL) != CL_SUCCESS) {
		char buffer[10240];
		clGetProgramBuildInfo(program, devices[0], CL_PROGRAM_BUILD_LOG, sizeof(buffer), buffer, NULL);
		fprintf(stderr, "CL Compilation failed:\n%s", buffer);
		abort();
	}

	const int GRIDSIZEX = 27;
	const int GRIDSIZEY = 17;

	Matrix h = Matrix(GRIDSIZEX, GRIDSIZEY);
	h.fill(120.0);
	h.print();

	Matrix Wx = Matrix(GRIDSIZEX, GRIDSIZEY);
	Wx.fill(0.0);

	Matrix Wy = Matrix(GRIDSIZEX, GRIDSIZEY);
	Wy.fill(10.0); //wind blowing along the y-axis at a speed of W = 10 ms-1

	Matrix pM = Matrix(GRIDSIZEX, GRIDSIZEY);
	pM.fill(0.0);

	int HSIZE = h.sizeX * h.sizeY;
	int NX = h.sizeX;
	int NY = h.sizeY;
	int step_nr = 0;

	cl_mem Wx_buffer = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(float)*HSIZE, Wx.getTab(), &_err);
	CL_CHECK_ERR(_err);

	cl_mem Wy_buffer = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(float)*HSIZE, Wy.getTab(), &_err);
	CL_CHECK_ERR(_err);

	cl_mem H_buffer = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(float)*HSIZE, h.getTab(), &_err);
	CL_CHECK_ERR(_err);

	cl_mem pM_buffer = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(float)*HSIZE, pM.getTab(), &_err);
	CL_CHECK_ERR(_err);

	cl_mem M_buffer = clCreateBuffer(context, CL_MEM_WRITE_ONLY, sizeof(float)*HSIZE, NULL, &_err);
	CL_CHECK_ERR(_err);

	cl_kernel kernel;
	kernel = clCreateKernel(program, "simple_demo", &_err);
	CL_CHECK_ERR(_err);
	CL_CHECK(clSetKernelArg(kernel, 0, sizeof(NX), &NX));
	CL_CHECK(clSetKernelArg(kernel, 1, sizeof(NY), &NY));
	CL_CHECK(clSetKernelArg(kernel, 2, sizeof(step_nr), &step_nr));
	CL_CHECK(clSetKernelArg(kernel, 3, sizeof(Wx_buffer), &Wx_buffer));
	CL_CHECK(clSetKernelArg(kernel, 4, sizeof(Wy_buffer), &Wy_buffer));
	CL_CHECK(clSetKernelArg(kernel, 5, sizeof(H_buffer), &H_buffer));
	CL_CHECK(clSetKernelArg(kernel, 6, sizeof(pM_buffer), &pM_buffer));
	CL_CHECK(clSetKernelArg(kernel, 7, sizeof(M_buffer), &M_buffer));

	cl_command_queue queue;
	queue = clCreateCommandQueue(context, devices[0], 0, &_err);
	CL_CHECK_ERR(_err);

	cl_event kernel_completion;
	cl_event copying_completion;
	printf("HSIZE = %d\n", HSIZE);
	size_t global_work_size[1] = { HSIZE };

	const int max_iterations = 10;

	bool stabilised = false;

	//for (step_nr = 0; step_nr != iterations; step_nr++) {
	while (!stabilised || step_nr < 10) {

		if (step_nr > max_iterations)
			break;

		CL_CHECK(clSetKernelArg(kernel, 2, sizeof(step_nr), &step_nr));
		CL_CHECK(clEnqueueNDRangeKernel(queue, kernel, 1, NULL, global_work_size, NULL, 0, NULL, &kernel_completion));
		CL_CHECK(clWaitForEvents(1, &kernel_completion));
		CL_CHECK(clReleaseEvent(kernel_completion));

		Matrix M = Matrix(GRIDSIZEX, GRIDSIZEY);

		CL_CHECK(clEnqueueReadBuffer(queue, M_buffer, CL_TRUE, 0, HSIZE * sizeof(float), M.getTab(), 0, NULL, NULL));

		printf("Result:\n");
		M.print();
		M.printToFile(resultsFileName);

		/*Matrix M = Matrix(GRIDSIZEX, GRIDSIZEY);
		CL_CHECK(clEnqueueReadBuffer(queue, M_buffer, CL_TRUE, 0, HSIZE * sizeof(float), M.getTab(), 0, NULL, NULL));

		Matrix pM = Matrix(GRIDSIZEX, GRIDSIZEY);
		CL_CHECK(clEnqueueReadBuffer(queue, pM_buffer, CL_TRUE, 0, HSIZE * sizeof(float), pM.getTab(), 0, NULL, NULL));*/

		//if (step_nr % 2 == 1) {
		//	stabilised = true;
		//	for (int x = 0; x != M.sizeX; x++) {
		//		for (int y = 0; y != M.sizeY; y++) {
		//			float diff = mabs(M.get(x, y) - pM.get(x, y));
		//			if (diff > 1e-5) {
		//				stabilised = false;
		//				//printf("step [%d]: diff = %.5f\n", step_nr, diff);
		//			}
		//		}
		//	}
		//}

		if (step_nr < max_iterations - 1) {
			CL_CHECK(clEnqueueCopyBuffer(queue, M_buffer, pM_buffer, 0, 0, sizeof(float)*HSIZE, 0, NULL, &copying_completion));
			CL_CHECK(clWaitForEvents(1, &copying_completion));
			CL_CHECK(clReleaseEvent(copying_completion));
		}
		step_nr++;
	}


	Matrix M = Matrix(GRIDSIZEX, GRIDSIZEY);

	CL_CHECK(clEnqueueReadBuffer(queue, M_buffer, CL_TRUE, 0, HSIZE * sizeof(float), M.getTab(), 0, NULL, NULL));
	
	printf("Result:\n");
	M.print();
	M.printToFile(resultsFileName);

	CL_CHECK(clReleaseMemObject(Wx_buffer));
	CL_CHECK(clReleaseMemObject(Wy_buffer));
	CL_CHECK(clReleaseMemObject(H_buffer));
	CL_CHECK(clReleaseMemObject(M_buffer));
	CL_CHECK(clReleaseMemObject(pM_buffer));

	CL_CHECK(clReleaseKernel(kernel));
	CL_CHECK(clReleaseProgram(program));
	CL_CHECK(clReleaseContext(context));

	system("pause");

	return 0;
}

