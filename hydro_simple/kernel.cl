//Note: arrays are in fact 1D, but we work on 2D grid
//
//IN:
//sizeX, sizeY - size of the grid
//step_nr - number of timestep
//H[][] - water depth
//Wx[][], Wy[][] - wind velocity components
//pM[][] - previous values of M[][]

//OUT:
//M[][] containing:
//Mx  - x-component of mass transport
//My  - y-component of mass transport
//ksi - sea level

__kernel void simple_demo(int sizeX, int sizeY, int step_nr, __global float *Wx, __global float *Wy, __global float *H,
	__constant float *pM, __global float *M)
{
	//bottom friction coefficient
	float R;

	//vertical eddy viscocisty
	float A;

	//Coriolis parameter
	const float omega = 1.12E-04;

	//acceleration due to gravity
	float g = 9.81;

	//tangential wind stress components
	float Tx;
	float Ty;

	//constant from Felzenbaum theory
	const float A_const = 4.7E-08;

	//gamma 
	const float gamma = 3.26E-06;

	int i = get_global_id(0);
	int x = i % sizeX;
	int y = i / sizeX;

	//wind velocity
	float W = sqrt(Wx[i] * Wx[i] + Wy[i] * Wy[i]);
	
	A = A_const * W * W / omega;
	R = M_PI * A / (4 * H[i] * H[i]);

	Tx = gamma * W * Wx[i];
	Ty = gamma * W * Wy[i];

	//time step [s]
	float tau = 60;

	//space step [m]
	float h = 10000;

	float val = pM[i];

	if (x > 0 && x < (sizeX - 1) && y > 0 && y < (sizeY - 1)) { //checking if not on border
		if (x % 2 == 0) {
			if (y % 2 == 1) {  
				if (step_nr % 2 == 0) { //checking parity of timestep
					//computing Mx
					float aMy = pM[i + 1 + sizeX] + pM[i - 1 + sizeX] + pM[i - 1 - sizeX] + pM[i + 1 - sizeX];
					val = pM[i] * (1 - 2 * R * tau) + 0.5 * omega * tau * aMy - 
						(g * tau * H[i] * (pM[i + 1] - pM[i - 1]) / h) + 2 * tau * Tx;
					//val = 1.0;
				}
			}
		} 
		else {                 
			if (y % 2 == 0) {  
				if (step_nr % 2 == 0) { //checking parity of timestep
					//computing My
					float aMx = pM[i + 1 + sizeX] + pM[i - 1 - sizeX] + pM[i + 1 - sizeX] + pM[i - 1 + sizeX];
					val = pM[i] * (1 - 2 * R * tau) - 0.5 * omega * tau * aMx - 
						(g * tau * H[i] * (pM[i + sizeX] - pM[i - sizeX]) / h) + 2 * tau * Ty;
					//val = 2.0;
				}
			}
			else {
				if (step_nr % 2 == 1) { //checking parity of timestep
					//computing ksi
					val = pM[i] - (tau / h) * (pM[i + 1] - pM[i - 1] + pM[i + sizeX] - pM[i - sizeX]);
					//val = 3.0;
				}
			}
		}
		M[i] = val; //updating grid cell with computed value 
				  //(or just reassigning the same value, if computations didn't occur)
	}

}