#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
class Matrix {

private:
	float *tab;

public:
	Matrix(void);
	Matrix(int sizeX, int sizeY);
	~Matrix(void);
	int sizeX;
	int sizeY;
	void fillRandomly();
	void fill(float val);
	void print();
	void set(int x, int y, float val);
	float get(int x, int y);
	float* getTab();
	void setTab(float* tab);
	void printToFile(char* fileName);
};