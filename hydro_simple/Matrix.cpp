#define _CRT_SECURE_NO_DEPRECATE

#include "Matrix.h"

Matrix::Matrix(void) {
}

Matrix::~Matrix() {
}

Matrix::Matrix(int sizeX, int sizeY) {
	this->sizeX = sizeX;
	this->sizeY = sizeY;
	tab = (float*)malloc(sizeof(float)* sizeX * sizeY);
}

void Matrix::set(int x, int y, float val) {
	tab[x + y * sizeX] = val;
}

float Matrix::get(int x, int y) {
	return tab[x + y * sizeX];
}

void Matrix::fillRandomly() {
	srand(time(NULL));
	for (int i = 0; i != sizeX; i++) {
		for (int j = 0; j != sizeY; j++) {
			int r = rand() % 100 + 1;
			set(i, j, (float)1 / r);
		}
	}
}

void Matrix::fill(float val) {
	for (int i = 0; i != sizeX; i++) {
		for (int j = 0; j != sizeY; j++) {
			set(i, j, val);
		}
	}
}

void Matrix::print() {
	for (int j = 0; j != sizeY; j++) {
		for (int i = 0; i != sizeX; i++) {
			printf("%12.6f ", get(i, j));
		}
		printf("\n");
	}
}

float* Matrix::getTab() {
	return tab;
}

void Matrix::setTab(float* tab) {
	this->tab = tab;
}

void Matrix::printToFile(char* fileName) {
	FILE *file;
	file = fopen(fileName, "a");
	for (int j = 0; j != sizeY; j++) {
		for (int i = 0; i != sizeX; i++) {
			fprintf(file, "%12.6f ", get(i, j));
		}
		fprintf(file, "\n");
	}
	fprintf(file, "\n\n");
	fclose(file);
}
