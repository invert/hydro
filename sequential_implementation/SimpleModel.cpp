#define _USE_MATH_DEFINES
#include<cstdio>
#include<cmath>
#include "Matrix.h"

#define N 5
#define TAU 60.0
#define h 10000.0


int main()
{
	const int GRID_SIZE_Y = 17;
	const int GRID_SIZE_X = 27;
	const double gamma = 3.26e-6;
	const double g = 9.80665;
	const double H = 120.0;
	const double OMEGA = 1.12e-4;
	const double W = 10.0;
	const double Wx = 0;
	const double Wy = W;
	const double R = M_PI * 4.7e-8 * (W * W / OMEGA) / (4 * H * H);
	double Tx = gamma * W * Wx;
	double Ty = gamma * W * Wy;

	double stability_criteria = -R *  h * h / (2 * g * H) + h / sqrt(4 * g * H);
	printf("Stability criteria %lf <= %lf\n", TAU, stability_criteria);
	printf("Critical depth H_cr = %lf\n", 8.7e-4 * W / OMEGA);
	printf("R = %.16lf\n", R);

	remove("test.dat");
	remove("mx.dat");
	remove("my.dat");


	Matrix Mx = Matrix(GRID_SIZE_X / 2 + 1 + 2, GRID_SIZE_Y / 2 + 2);
	Matrix My = Matrix(GRID_SIZE_X / 2 + 2, GRID_SIZE_Y / 2 + 1 + 2);
	Matrix sea_level = Matrix(GRID_SIZE_X / 2 + 2, GRID_SIZE_Y / 2 + 2);
	Matrix prev_Mx = Matrix(GRID_SIZE_X / 2 + 1 + 2, GRID_SIZE_Y / 2 + 2);
	Matrix prev_My = Matrix(GRID_SIZE_X / 2 + 2, GRID_SIZE_Y / 2 + 1 + 2);
	Matrix prev_sea_level = Matrix(GRID_SIZE_X / 2 + 2, GRID_SIZE_Y / 2 + 2);

	int step = 0;
	bool stabilised = false;
	while (stabilised == false || step < 10)
	{
		if (step % 2 == 0)
		{
			for (int i = 1; i < Mx.size_y - 1; i++)
				for (int j = 1; j < Mx.size_x - 1; j++)
					Mx.values[i][j] = prev_Mx.values[i][j] * (1 - 2 * R * TAU) +
						0.5 * OMEGA * TAU * (prev_My.values[i][j] + prev_My.values[i + 1][j] + 
						prev_My.values[i][j - 1] + prev_My.values[i + 1][j - 1]) - 
						(g * TAU * H * (prev_sea_level.values[i][j] - prev_sea_level.values[i][j - 1]) / h) + 
						2 * TAU * Tx;
			for (int i = 1; i < My.size_y - 1; i++)
				for (int j = 1; j < My.size_x - 1; j++)
					My.values[i][j] = prev_My.values[i][j] * (1 - 2 * R * TAU) -
						0.5 * OMEGA * TAU * (prev_Mx.values[i - 1][j] + prev_Mx.values[i - 1][j + 1] +
						prev_Mx.values[i][j] + prev_Mx.values[i][j + 1]) -
						(g * TAU * H * (prev_sea_level.values[i][j] - prev_sea_level.values[i - 1][j]) / h) +
						2 * TAU * Ty;
			prev_Mx.set(Mx.values);
			prev_My.set(My.values);
			//prev_Mx.printToFile("mx.dat", step);
			//prev_My.printToFile("my.dat", step);
		}
		else
		{
			stabilised = true;
			for (int i = 1; i < sea_level.size_y - 1; i++)
			{
				for (int j = 1; j < sea_level.size_x - 1; j++)
				{
					sea_level.values[i][j] = prev_sea_level.values[i][j] -
						TAU / h * (prev_Mx.values[i][j + 1] - prev_Mx.values[i][j] +
						prev_My.values[i + 1][j] - prev_My.values[i][j]);
					if (abs(sea_level.values[i][j] - prev_sea_level.values[i][j]) > 1e-5)
						stabilised = false;
				}
			}
			prev_sea_level.set(sea_level.values);
			//sea_level.print();
			sea_level.printToFile("test.dat", step);
			printf("Step[%d] - average value: %.12lf\n", step, sea_level.average());
		}
		step++;
	}
	return 0;
}