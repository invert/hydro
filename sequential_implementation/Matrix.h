#pragma once
#include<cstdio>

class Matrix
{
private:

public:
	double **values;
	int size_x;
	int size_y;
	Matrix(int size_x, int size_y);
	~Matrix();
	void print();
	void printToFile(char *fileName, int step);
	void set(double **values);
	double average();
};

