#include "Matrix.h"

Matrix::Matrix(int size_x, int size_y)
{
	this->size_x = size_x;
	this->size_y = size_y;
	this->values = new double*[size_y]();
	for (int i = 0; i < size_y; i++)
		this->values[i] = new double[size_x]();
}


Matrix::~Matrix()
{
	delete(this->values);
}

void Matrix::print()
{
	for (int i = 0; i < this->size_y; i++)
	{
		for (int j = 0; j < this->size_x; j++)
			printf("%12.5lf ", this->values[i][j]);
		printf("\n");
	}
}

void Matrix::printToFile(char* fileName, int step)
{
	FILE *file;
	file = fopen(fileName, "a");
	fprintf(file, "Step[%d] - average value: %.12lf\n", step, average());
	for (int i = 0; i < this->size_y; i++) {
		for (int j = 0; j != this->size_x; j++) {
			fprintf(file, "%12.5lf ", this->values[i][j]);
		}
		fprintf(file, "\n");
	}
	fclose(file);
}

void Matrix::set(double **values)
{
	for (int i = 0; i < this->size_y; i++)
		for (int j = 0; j < this->size_x; j++)
			this->values[i][j] = values[i][j];
}

double Matrix::average()
{
	double sum = 0;
	for (int i = 0; i < this->size_y; i++)
		for (int j = 0; j < this->size_x; j++)
			sum += this->values[i][j];
	return sum /= (this->size_y - 2) * (this->size_x - 2);
}